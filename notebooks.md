# Notebooks

This is a curated index of all available Jupyter Notebooks containing teaching materials. For a better understanding of the context of each notebook, please refer to our [Educational Stories](educational-stories.md).

[Course specific](#Courses) material can be found at the bottom of this page.

## Machine Learning Fundamentals

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td><a href="notebooks/machine-learning-fundamentals/probability-theory/bayes-theorem.ipynb">probability-theory/bayes-theorem</a></td>
        <td>Benjamin Voigt</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/8">Christoph Jansen</a></td>
        <td>not ready</td>
    </tr>
    <tr>
        <td><a href="notebooks/machine-learning-fundamentals/probability-theory/exercise-cookie-problem.ipynb">probability-theory/exercise-cookie-problem</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/5">Benjamin Voigt</a></td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/10">unassigned</a></td>
    </tr>
    <tr>
        <td><a href="notebooks/machine-learning-fundamentals/probability-theory/bayesian-networks-by-example.ipynb">probability-theory/bayesian-networks-by-example</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/6">Benjamin Voigt</a></td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/11">unassigned</a></td>
    </tr>
    <tr>
        <td><a href="notebooks/machine-learning-fundamentals/probability-theory/exercise-d-separation.ipynb">probability-theory/exercise-d-separation</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/9">unassigned</a></td>
        <td>not ready</td>
    </tr>
    <tr>
        <td><a href="notebooks/machine-learning-fundamentals/exercise-simple-linear-regression.ipynb">exercise-simple-linear-regression</a></td>
        <td>Christian Herta, Benjamin Voigt</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/16">unassigned</a></td>
        <td>not ready</td>
    </tr>
</table>

## Medical Image Classification

These notebooks are not yet available, please check back later for more materials...

## Robotic Autonomous Driving

These notebooks are not yet available, please check back later for more materials...

## Text Information Extraction

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td><a href="notebooks/text-information-extraction/data-exploration/germ-eval-2014.ipynb">data-exploration/germ-eval-2014</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/7">Benjamin Voigt</a></td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/12">unassigned</a></td>
    </tr>
    <tr>
        <td><a href="notebooks/text-information-extraction/sequences/exercise-bi-gram-language-model.ipynb">sequences/exercise-bi-gram-language-model</a></td>
        <td>Christoph Jansen, Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/15">unassigned</a></td></td>
        <td>not ready</td>
    </tr>
</table>


## Uncategorized

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td><a href="notebooks/blueprint.ipynb">blueprint</a></td>
        <td>Benjamin Voigt, Christoph Jansen</td>
        <td>not applicable</td>
        <td>not applicable</td>
    </tr>
</table>


## Courses

**HTW-Berlin - Informatik und Wirtschaft - Aktuelle Trends der Informations- und Kommunikationstechnik**

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td><a href="slides/courses/htw-berlin/informatik-und-wirtschaft/aktuelle-trends/linear-regression.pdf">linear-regression (PDF slides)</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/17">unassigned</a></td>
        <td>not ready</td>
    </tr>
    <tr>
        <td><a href="notebooks/courses/htw-berlin/informatik-und-wirtschaft/aktuelle-trends/linear-regression-exercise.ipynb">linear-regression-exercise</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/18">unassigned</a></td>
        <td>not ready</td>
    </tr>
    <tr>
        <td><a href="slides/courses/htw-berlin/informatik-und-wirtschaft/aktuelle-trends/logistic-regression.pdf">logistic-regression (PDF slides)</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/19">unassigned</a></td>
        <td>not ready</td>
    </tr>
    <tr>
        <td><a href="notebooks/courses/htw-berlin/informatik-und-wirtschaft/aktuelle-trends/logistic-regression-exercise.ipynb">logistic-regression-exercise</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/20">unassigned</a></td>
        <td>not ready</td>
    </tr>
    <tr>
        <td><a href="slides/courses/htw-berlin/informatik-und-wirtschaft/aktuelle-trends/evaluation.pdf">evaluation (PDF slides)</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/21">unassigned</a></td>
        <td>not ready</td>
    </tr>
    <tr>
        <td><a href="notebooks/courses/htw-berlin/informatik-und-wirtschaft/aktuelle-trends/evaluation-exercise.ipynb">evaluation-exercise</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/22">unassigned</a></td>
        <td>not ready</td>
    </tr>
</table>
