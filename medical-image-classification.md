# Medical Image Classification Scenario

This project contains Python Jupyter notebooks to teach machine learning content in the context of medical data, e.g., automated tumor detection. The material focuses primarily on teaching basic knowledge of convolutional neural networks but also contains portions of fundamental machine learning knowledge. As an advanced topic, you get an introduction to the concept of attention models.

## Scenario Description

The usual way to diagnose breast cancer is to analyze a patient's tissue samples under a microscope. Examining such tissue slides is a complex task that requires years of training and expertise in a specific area by a pathologist. But scientific studies show that even in a group of highly experienced experts there can be substantial variability in the diagnoses for the same patient, which indicates the possibility of misdiagnosis \[ELM15\]\[ROB95\]. This result is not surprising given the enormous amount of details in a tissue slide at 40x magnification. To get a sense of the amount of data, imagine that an average digitized tissue slide has a size of 200.000x100.000 pixel and you have to inspect every one of them to get an accurate diagnose. Needless to say, if you have to examine multiple slides per patient and have several patients,  this is a lot of data to cover in a usually limited amount of diagnosis time. Following image depicts a scanned tissue slide (whole slide image, short WSI) at different magnification level.

![WSI example](media/voigt/images/wsi_example.png)

Under such circumstances, an automated detection algorithm can naturally complement the pathologists’ work process to enhance the possibility of an accurate diagnosis. Such algorithms have been successfully developed in the scientific field in recent years, in particular, models based on Convolutional Neural Networks \[WAN16\]\[LIU17\]. But getting enough data to train machine learning algorithms is still a challenge in the medical context. However, the Radboud University Medical Center (Nijmegen, the Netherlands) and the University Medical Center Utrecht (Utrecht, the Netherlands) provide an extensive dataset containing sentinel lymph nodes of breast cancer patients in the context of their CAMELYON16 challenge. These data provide a good starting point for further scientific investigations and are therefore mainly used in that scenario. You can get the raw data as a registered user of the [CAMELYON17 challenge](https://camelyon17.grand-challenge.org/download/) (GoogleDrive/Baidu) or create a custom dataset for your own needs with the [CVEDIA](https://cvedia.com/dataset/profile/camelyon-16) platform.

In context of the medical scenario you will develop a custom classifier for the given medical dataset that will decide:
1. If a lymph node tissue contains metastases.
2. What kinds of metastases are present, e.g., micro- or macro-metastasis?
3. Which pN stage the patient is in based on the [TNM staging system](https://en.wikipedia.org/wiki/TNM_staging_system)?

## Teaching Material

Detection of metastases is a classification problem. To solve this first issue, you will implement a classification pipeline based on Neural Networks (NN) and develop it to Convolutional Neural Network (CNN)[LEC98]. Further, you will extend that pipeline with classical machine learning approaches, like decision trees, to address the second and third issue.

We divide the teaching material in that scenario into *WSI preprocessing* (global operations, like i/o handling of WSI data), *WSI postprocessing* (building heatmaps, extract features, preparation for further classification) and classification (machine learning approaches to solve our scenario issues) in order to make it modular and reusable. Besides, you will examine the concept of soft attention \[MIN14\]\[XU15\] on WSIs to see if the amount of processed data can be reduced that way.

The deep.TEACHING project provides educational material for students to gain basic knowledge about the problem domain, the programming, math and statistics requirements, as well as the mentioned algorithms and their evaluation. Students will also learn how to construct complex machine learning systems, which can incorporate several algorithms at once.

## Notebooks

Topics are tentative and subject to change.

- WSI Preprocessing
  - Reading different WSIs formats
  - Extract tissue from WSIs and create a custom dataset
  - Apply data augmentation and further task individual data transformations
  - Exploring different color models in context of medical data analysis
- WSI Postprocessing
  - WSI confidence maps as classification result
  - Morphological operations to improve classsification results
  - Extract useful features from confidence maps to train further classifiers
- Classification
  - Neural Networks as image classifier
  - Convolutional Neural Networks image classifier
  - Recurrent Neural Networks and Convolutional Neural Networks for image segmentation
  - Decision tree (random forest, mlp)
  - Cluster analysis (k-means, Expectation-Maximization(EM))
- Soft-Attention
  - Understand and rebuild [MIN14]
  - Apply soft attention concept to WSIs

## Reference (ISO 690)

<table>
    <tr>
        <td>
            [ELM15]
        </td>
        <td>
            ELMORE, Joann G., et al. Diagnostic concordance among pathologists interpreting breast biopsy specimens. Jama, 2015, 313. Jg., Nr. 11, S. 1122-1132.
        </td>
    </tr>
    <tr>
        <td>
            [LEC98]
        </td>
        <td>
            LeCun, Yann, et al. "Gradient-based learning applied to document recognition." Proceedings of the IEEE 86.11 (1998): 2278-2324.
        </td>
    </tr>
    <tr>
        <td>
            [LIU17]
        </td>
        <td>
            LIU, Yun, et al. Detecting cancer metastases on gigapixel pathology images. arXiv preprint arXiv:1703.02442, 2017.
        </td>
    </tr>
    <tr>
        <td>
            [MIN14]
        </td>
        <td>
            MNIH, Volodymyr, et al. Recurrent models of visual attention. In: Advances in neural information processing systems. 2014. S. 2204-2212.
        </td>
    </tr>
    <tr>
        <td>
            [ROB95]
        </td>
        <td>
            ROBBINS, P., et al. Histological grading of breast carcinomas: a study of interobserver agreement. Human pathology, 1995, 26. Jg., Nr. 8, S. 873-879.
        </td>
    </tr>
    <tr>
        <td>
            [WAN16]
        </td>
        <td>
            WANG, Dayong, et al. Deep learning for identifying metastatic breast cancer. arXiv preprint arXiv:1606.05718, 2016.
        </td>
    </tr>
    <tr>
        <td>
            [XU15]
        </td>
        <td>
             XU, Kelvin, et al. Show, attend and tell: Neural image caption generation with visual attention. In: International Conference on Machine Learning. 2015. S. 2048-2057.
        </td>
    </tr>
</table>
