# Team

These are the team members of the deep.TEACHING project.

## Project Management

[Prof. Dr. Gefei Zhang](https://www.htw-berlin.de/hochschule/personen/person/?eid=10755)

## Project Supervisors

[Prof. Dr. Christian Herta](https://www.htw-berlin.de/hochschule/personen/person/?eid=2605)

[Prof. Dr. Peter Hufnagl](https://www.htw-berlin.de/hochschule/personen/person/?eid=1409)

## Research Assistants

[Patrick Bauman](https://www.htw-berlin.de/hochschule/personen/person/?eid=11706)

[Benjamin Voigt](https://www.htw-berlin.de/hochschule/personen/person/?eid=10919)

[Christoph Jansen](https://www.htw-berlin.de/hochschule/personen/person/?eid=9225)
