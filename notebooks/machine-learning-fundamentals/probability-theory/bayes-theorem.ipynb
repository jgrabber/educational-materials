{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ML-Fundamentals - Probabilty Theory - Bayes' Theorem "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Table of Contents\n",
    "* [Why is Bayes' Theorem of interest](#Why-is-Bayes'-Theorem-of-interest)\n",
    "* [Bayes' Theorem](#Bayes'-Theorem)\n",
    "  * [Visual Derivation](#Visual-Derivation)\n",
    "  * [Cookie Problem Example](#Cookie-Problem-Example)\n",
    "  * [Plain Mathematical Derivation](#Plain-mathematical-derivation)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)\n",
    "* [Acknowledgements](#Acknowledgements)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Why is Bayes' Theorem of Interest\n",
    "\n",
    "Bayesian inference lets you draw stronger conclusions from your data by folding in what you already know about the answer and Bayes' Theorem is the fundamental mathematical rule it relies on. This is the long term goal, but let us have a look at some examples first where Bayes' Theorem can provide an answer. \n",
    "\n",
    "- You had a new developed test to detect a seriouse disease and it came back positive. The doctor tells you that the prevalence is at 0,01% and the test indicates the presence of disease at 95% if the disease is indeed present. What is the probability that you have the disease?\n",
    "\n",
    "- At \"Südkreuz\" S-Bahn station in Berlin, an automatic face regonition system was installed to identify searched terrorists. The accuracy of the system is 99,9% and security experts estimate that of 4.200.000 people using the station in one year, about 13 have a terroristic background. In 0,0009% of the cases a non searched person is identified as a terrorist. What is the probability of the system reporting a detection that it is really a searched terrorist?\n",
    "\n",
    "In this notebook you will learn to solve such problems with Bayes' Theorem. You will learn prerequisite to derive Bayes' theorem and how it can be visulized. As a side note, Bayes' Theorem is often called Bayes' rule, Bayes' law or Bayesian Probability. So do not be confused if you read on of these terms in another source."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bayes' Theorem\n",
    "\n",
    "We start with the formula you find most often when you search for Bayes' theorem to make our goal clearer. But then we will toss it to the side for a moment and take a look at some fundamentals of probability theory from a visual perspective. \n",
    "\n",
    "\\begin{equation}\n",
    "    P(A \\mid B) = \\frac{P(B \\mid A)*P(A) }{P(B)} \n",
    "\\end{equation}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visual derivation\n",
    "\n",
    "Suppose we have a __Universe__ $\\Omega$ that represents all possible outcomings, e.g. of an experiment or a prediction. We call one possible outcome an __event__ $A$ in our Universe $\\Omega$. In a concrete example, the universe could be made up of people participating in a scientific study on chocolate addiction. Event $A$ could represent all people that are addicted to chocolate. In the counterpart, all other individuals describe the remaining population of $\\Omega$ (formally $\\neg A$).\n",
    "\n",
    "<center>\n",
    "  <img src=\"../../../media/voigt/images/Bayes-Theorem_Event-A.png\">\n",
    "</center>\n",
    "\n",
    "So what is the probability a person is addicted to chocolate if you choose it at random? Probability theory tells us, it is the number of elements in our observed event $A$ (cardinality of event $A$) divided by the number of all elements of our universe $\\Omega$ (cardinality of $\\Omega$). \n",
    "\n",
    "\\begin{equation}\n",
    "    P(A) = \\frac{\\left | A \\right |}{\\left | \\Omega \\right |} \n",
    "\\end{equation}\n",
    "\n",
    "In the same way, further events can be added. In our study, a new test will be evaluated that detects the chocolate addiction. Let us call it event $B$. Event $B$ includes all people with a 'positive' test result which indicates an addiction. We can visualize it and calculate the probability $p(B)$ like we did with event $A$. So the probability $p(B)$ of a person with a positive test result randomly selected from our population would be the cardinality of event $B$ divided by the cardinality of universe $\\Omega$.\n",
    "\n",
    "<center>\n",
    "  <img src=\"../../../media/voigt/images/Bayes-Theorem_Event-B.png\">\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far we have considered the events independently and looked at them one after another. Let us change that. As you can see, event $A$ and event $B$ share some people of our study population. From here it becomes more interesting because complex problem statements are possible. \n",
    "\n",
    "<center>\n",
    "  <img src=\"../../../media/voigt/images/Bayes-Theorem_Event-AandB.png\">\n",
    "</center>\n",
    "\n",
    "One simple question is what is the probability that both events occur $P(A \\cap B)$. To put it in words: the probability of a _'random selected person is addicted to chocolate and has a positive test'_. $P(A \\cap B)$ is the cardinality of the intersection of the two events divided by the cardinality of the universe.\n",
    "\n",
    "$$\n",
    "  P(A \\cap B) = \\frac{\\left | A \\cap B \\right |}{\\left | \\Omega \\right |} \n",
    "$$\n",
    "\n",
    "But there is more information in the diagram. Another question that leads us to Bayes' Theorem is _'if a person got a positive test, what is the probability he or she is addicted?'_. The mathematical notation for such a question is a conditional probability $P(A \\mid B)$, the probability of event $A$ given event $B$. You can visualize it by reducing the area of our diagram to the region of event $B$ and ask for the for the intersection of $A \\cap B$.  \n",
    "\n",
    "<center>\n",
    "  <img src=\"../../../media/voigt/images/Bayes-Theorem_Event-A-Reduced.png\">\n",
    "</center>\n",
    "\n",
    "From the visualization it becomes clear that the following applies:\n",
    "\n",
    "$$\n",
    "  P(A \\mid B) = \\frac{\\left | A \\cap B \\right |}{\\left | B \\right |}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But we changed our perspective here from the universe $\\Omega$ to our event $B$. To get back to the probabilities from the universe we divide the numerator and the denominator by the cardinality of $\\Omega$, $|\\Omega|$. \n",
    "\n",
    "$$\n",
    "  P(A \\mid B) = \\frac{\\frac{\\left | A \\cap B \\right |}{|\\Omega|}}{\\frac{\\left | B \\right |}{|\\Omega|}}\n",
    "$$\n",
    "\n",
    "We can do that too for the conditional probability $P(B \\mid A)$ to get a similar equation and diagram. $P(B \\mid A)$ represents _'given that a randomly selected person is addicted, what is the probability that the person gets a positive test result?_. \n",
    "\n",
    "<center>\n",
    "  <img src=\"../../../media/voigt/images/Bayes-Theorem_Event-B-Reduced.png\">\n",
    "</center>\n",
    "\n",
    "We can transform the equation a bit to get $P(A \\cap B)$:\n",
    "\n",
    "$$\n",
    " P(B \\mid A) = \\frac{P(A \\cap B)}{P(A)}\n",
    "$$\n",
    "\n",
    "$$\n",
    " \\Leftrightarrow P(A \\cap B) = P(B \\mid A)*P(A)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another aspect recognizable from the combination of both diagrams is that $P(A \\cap B)$ and $P(B \\cap A)$ have the same probability and therefore are interchangeable. Now we have all the components (joint and conditional probabilities) for the derivation of Bayes' Theorem together.\n",
    "\n",
    "\\begin{equation}\n",
    " P(A \\cap B) = P(B \\cap A)\n",
    "\\end{equation}\n",
    "\n",
    "Insert the equations we found during our visual derivation.\n",
    "\n",
    "\\begin{equation}\n",
    " P(A \\mid B) * P(B) =  P(B \\mid A) * P(A)\n",
    "\\end{equation}\n",
    "\n",
    "Transform the equation for the conditional probability $P(A \\mid B)$.\n",
    "\n",
    "\\begin{equation}\n",
    " P(A \\mid B) = \\frac{P(B \\mid A) * P(A)}{P(B)}\n",
    "\\end{equation}\n",
    "\n",
    "Here we are Bayes' Theorem. Let us have a look at one simple example to see how we can apply the equation in practice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cookie Problem Example\n",
    "\n",
    "This problem is from the book 'Think Bayes' by Allen B. Downey [DOW13](DOW13) and was based on an example from Wikipedia. Suppose you have two bowls and both contains two types of cookies but with a diffrent distribution. Bowl 1 contains 30 vanilla cookies and 10 chocolate. Bowl 2 contains 20 cookies of each kind.\n",
    "\n",
    "<center>\n",
    "  <img src=\"../../../media/voigt/images/Bayes-Theorem_Bowl.png\">\n",
    "</center>\n",
    "\n",
    "Now you randomly draw a cookie from a bowl without looking at the bowls. It is a vanilla cookie. But what is the probability that the cookie came from bowl 1? Mathematically we can express that question as a conditional probability:\n",
    "\n",
    "\\begin{equation}\n",
    " P(bowl\\;1 \\mid vanilla\\;cookie) \n",
    "\\end{equation}\n",
    "\n",
    "Well, a conditional probability is one term of our Bayes' Theorem equation. Let us take a look at the whole equation again and see if it helps us solve our cookie problem.\n",
    "\n",
    "<center>\n",
    "  <img src=\"../../../media/voigt/images/Bayes-Theorem_Cookie_Equation.png\">\n",
    "</center>\n",
    "\n",
    "We can derive all probabilities we need to solve the equation from the discription above.  \n",
    "\n",
    "- $P(vanilla\\;cookie \\mid bowl\\;1)$: Is the probability that we get a vanilla cookie from bowl 1, which is exactly $3/4$\n",
    "\n",
    "- $P(bowl\\;1)$: Represent the probability to choose $bowl\\;1$. Since we grabed a cookie from a bowl without looking at them, we choosed the bowl at random. Therefore we can assume both bowls have the same likelihood of $1/2 = P(bowl\\;1) = P(bowl\\;2)$.\n",
    "\n",
    "- $P(vanilla\\;cookie)$: This is the probability of drawing a vanilla cookie from either bowl. Since we assumed that both bowls are equally likely and both contain the same number of cookies, we had the same chance of drawing any cookie. In total, we have 80 cookies and 50 of them are vanilla cookies, so $P(vanilla\\;cookie)=50/80=5/8$.\n",
    "\n",
    "Giving all information we can solve our initial question \n",
    "\n",
    "\\begin{equation}\n",
    " P(bowl\\;1 \\mid vanilla\\;cookie) = \\frac{P(vanilla\\;cookie \\mid bowl\\;1) * P(bowl\\;1)}{P(vanilla\\;cookie)}\n",
    "\\end{equation}\n",
    "\n",
    "\\begin{equation}\n",
    " P(bowl\\;1 \\mid vanilla\\;cookie) = \\frac{(3/4) * (1/2)}{(5/8)}\n",
    "\\end{equation}\n",
    "\n",
    "\\begin{equation}\n",
    " P(bowl\\;1 \\mid vanilla\\;cookie) = 3/5\n",
    "\\end{equation}\n",
    "\n",
    "As exercise you can implement the cookie problem in the deep.TEACHING notebook [Machine Learning Fundamentals - Probability Theory - Exercise: Cookie Problem](https://gitlab.com/deep.TEACHING/educational-materials/blob/master/notebooks/machine-learning-fundamentals/probability-theory/exercise-cookie-problem.ipynb) \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plain mathematical derivation\n",
    "\n",
    "Various textbooks give the mathematical derivation of Bayes's theorem. We follow the derivation of [FAH11](FAH11) and recommend [MUR12](MUR12) as another international source.\n",
    "\n",
    "For two given events $A,B \\subset \\Omega$ and $P(B) > 0$ the **conditional probability** is definied as  \n",
    "\n",
    "\\begin{equation}\n",
    " P(A \\mid B) = \\frac{P(A \\cap B)}{P(B)}\n",
    "\\end{equation}\n",
    "\n",
    "From the definition of the conditional probability and the symetry ability of joint probabilites, the **product rule** can be derived and written as\n",
    "\n",
    "\\begin{equation}\n",
    " P(A \\cap B) = P(B \\mid A) * P(A)\n",
    "\\end{equation}\n",
    "\n",
    "Combining the definition of conditional probabilities and the product rule yields to Bayes' Theorem:\n",
    "\n",
    "\\begin{equation}\n",
    "  P(A \\mid B) = \\frac{P(B \\mid A) * P(A)}{P(B)}\n",
    "\\end{equation}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Summary and Outlook\n",
    "In this notebook you got an introduction to Bayes' Theorem from a visual perspective. You got to know the concept of probabilities, joint probabilities and conditional probabilities via Venn diagrams and finally derived the equation of Bayes' Theorem.\n",
    "\n",
    "You can deepen your knowledge of Bayes' Theorem in offered exercises, e.g., [Machine Learning Fundamentals - Probability Theory - Exercise: Cookie Problem](https://gitlab.com/deep.TEACHING/educational-materials/blob/master/notebooks/machine-learning-fundamentals/probability-theory/exercise-cookie-problem.ipynb). To learn more about how Bayes' Theorem is used in Machine Learning you can take a look at the notebook [Machine Learning Fundamentals - Probability Theory - Bayesian inference]().\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"DOW13\"></a>[DOW13]\n",
    "        </td>\n",
    "        <td>\n",
    "            A. B. Downey, Think Bayes. Sebastopol, CA: O'Reilly Media, Inc., 2013\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"FAH\"></a>[FAH16]\n",
    "        </td>\n",
    "        <td>\n",
    "           Fahrmeir, Ludwig, et al. Statistik: Der Weg zur Datenanalyse. Springer-Verlag, 2016.\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"MUR12\"></a>[MUR12]\n",
    "        </td>\n",
    "        <td>\n",
    "           Kevin P. Murphy, Machine Learning: A Probabilistic Perspective. The MIT Press, 2012.\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Licenses\n",
    "\n",
    "## Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "ML-Fundamentals - Probabilty - Bayes' Theorem <br/>\n",
    "by [Benjamin Voigt](https://gitlab.com/bensnajdar) <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "## Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Benjamin Voigt\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Acknowledgements\n",
    "\n",
    "The deep.TEACHING notebooks are developed at [CBMI](http://cbmi.f4.htw-berlin.de/), a research institute of [HTW Berlin - University of Applied Sciences](https://www.htw-berlin.de/).\n",
    "The work is supported by the [German Ministry of Education and Research (BMBF)](https://www.bmbf.de/).\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
