---
hideToc: true
---

# Imprint

Hochschule für Technik und Wirtschaft Berlin<br/>
[Prof. Dr. Gefei Zhang](https://www.htw-berlin.de/hochschule/personen/person/?eid=10755)<br/>
Wilhelminenhofstraße 75A<br/>
12459 Berlin

E-Mail: [Gefei.Zhang@HTW-Berlin.de](mailto:Gefei.Zhang@HTW-Berlin.de)
